# SFML Build Image

A build image for a private project, which supports both Linux and MinGW-W64 builds
of C++ programs with the following packages:

* Ubuntu 20.10
* SFML v2.5.1
* TGUI v0.9.1
* Thor v2.0
* Lua v5.1.5
* ENet v1.3.13
* build-essential, cmake, and misc tools from Ubuntu's repositories.
