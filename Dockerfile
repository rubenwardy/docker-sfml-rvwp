from ubuntu:20.10

COPY install.sh /install
COPY install_linux.sh /install_linux
COPY install_mingw.sh /install_mingw
COPY toolchain-mingw64.cmake /toolchain-mingw64.cmake

RUN /install
