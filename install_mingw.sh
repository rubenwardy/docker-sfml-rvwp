#!/bin/bash

set -e

apt-get install -y mingw-w64


mkdir -p /tmp/rvwpwin
cd /tmp/rvwpwin

# Fix default gcc to support std::thread
if [[ -e /usr/bin/x86_64-w64-mingw32-gcc-posix ]]; then
	mv /usr/bin/x86_64-w64-mingw32-gcc /usr/bin/x86_64-w64-mingw32-gcc.tmp
    ln -s /usr/bin/x86_64-w64-mingw32-gcc-posix /usr/bin/x86_64-w64-mingw32-gcc
	mv /usr/bin/x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++.tmp
    ln -s /usr/bin/x86_64-w64-mingw32-g++-posix /usr/bin/x86_64-w64-mingw32-g++
fi

TOOLSET="x86_64-w64-mingw32"
MINGW_BIN_PREFIX="${TOOLSET}-"
MINGW_INSTALL_DIR="/usr/local/mingw64"   #"/usr/x86_64-w64-mingw32"
AR=${MINGW_BIN_PREFIX}ar
CC=${MINGW_BIN_PREFIX}gcc
RANLIB=${MINGW_BIN_PREFIX}ranlib
WINDRES=${MINGW_BIN_PREFIX}windres
PREFIX=${MINGW_INSTALL_DIR}
CROSSPATH="/usr/${TOOLSET}"


mkdir /usr/local/mingw64
mkdir /usr/local/mingw64/include
mkdir /usr/local/mingw64/lib

cd /tmp/rvwpwin\

# SFML
git clone https://github.com/SFML/SFML/ -b 2.5.1 --depth 1
cd SFML
cp -r extlibs/* /usr/local/mingw64/
cp -r /usr/local/mingw64/headers/* /usr/local/mingw64/include/
cp -r /usr/local/mingw64/libs-mingw/x64/* /usr/local/mingw64/lib/
rm -r /usr/local/mingw64/libs-*
cmake . \
    -DCMAKE_TOOLCHAIN_FILE=/toolchain-mingw64.cmake \
    -DCMAKE_INSTALL_PREFIX=/usr/local/mingw64 \
    -DCMAKE_BUILD_TYPE=Release
make install
cd /tmp/rvwpwin

# Thor
git clone https://github.com/Bromeon/Thor -b v2.0 --depth 1
cd Thor
cmake . \
    -DCMAKE_TOOLCHAIN_FILE=/toolchain-mingw64.cmake \
    -DCMAKE_INSTALL_PREFIX=/usr/local/mingw64 \
    -DCMAKE_BUILD_TYPE=Release
make install
cd /tmp/rvwpwin

# TGUI
git clone https://github.com/texus/TGUI -b v0.9.1

pushd TGUI
cmake . \
    -DCMAKE_TOOLCHAIN_FILE=/toolchain-mingw64.cmake \
    -DCMAKE_INSTALL_PREFIX=/usr/local/mingw64 \
    -DCMAKE_BUILD_TYPE=Release \
    -DTGUI_BUILD_GUI_BUILDER=0
make install
popd
cd /tmp/rvwpwin

# Lua
wget https://www.lua.org/ftp/lua-5.1.5.tar.gz -O lua.tar.gz
tar -xzf lua.tar.gz
cd lua-5.1.5
make -j3 LUA_A="liblua5.1.a" LUA_T="lua.exe" \
CC="$CC" AR="$AR rcu" RANLIB="$RANLIB" mingw

make install RANLIB="$RANLIB" INSTALL_TOP="$MINGW_INSTALL_DIR" \
INSTALL_INC="$MINGW_INSTALL_DIR/include/lua5.1/" \
TO_BIN="lua.exe luac.exe" TO_LIB="liblua5.1.a"
cd /tmp/rvwpwin


# Enet
wget http://enet.bespin.org/download/enet-1.3.14.tar.gz
tar -xzf enet-1.3.14.tar.gz
cd enet-1.3.14
cp -r include/* /usr/local/mingw64/include/
cp  enet64.lib /usr/local/mingw64/lib/enet.lib


cd /tmp/rvwpwin

# Set permissions
chmod -R +rx /usr/local/mingw64

echo "Done!"
