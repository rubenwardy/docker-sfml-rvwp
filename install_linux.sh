#!/bin/bash

set -e

# Install basic dependencies
apt-get install -y lua5.1 liblua5.1-dev libenet-dev


#
# Build specific dependencies
#


mkdir -p /tmp/build
pushd /tmp/build


# SFML

git clone https://github.com/SFML/SFML -b 2.5.1 --depth 1

pushd SFML

apt-get build-dep -y libsfml-dev

echo building make for dynamic release
cmake . -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=TRUE
make -j2
make install

popd


# Thor

wget http://www.bromeon.ch/libraries/thor/download/v2.0/thor-v2.0-sdk.7z
7z x thor-v2.0-sdk.7z

pushd thor-v2.0-sdk
cmake .
make -j3
make install
popd


# TGUI

git clone https://github.com/texus/TGUI -b v0.9.1

pushd TGUI
cmake . -DTGUI_BUILD_GUI_BUILDER=FALSE
make -j3
make install
popd
