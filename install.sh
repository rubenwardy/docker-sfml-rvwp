#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive

cp /etc/apt/sources.list /etc/apt/sources.list~
sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list
apt-get update

apt-get install -y zip unzip curl tar git-core p7zip-full wget lcov build-essential cmake python3 python3-venv
apt-get install -y clang clang-tidy clang-format luarocks valgrind

luarocks install luacheck
luarocks install busted

ln -s /usr/bin/python3 /usr/bin/python

/install_linux
/install_mingw

# Clean temporary files
rm -rf /tmp/*
